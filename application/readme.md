<h1>Sample project</h1>

<h3>Project features</h3>
1. Lists invoices and additionally converts by exchange rates values to the day when invoice was issued


   ```/invoice/list```
   
2. Allows to download invoices list in two formats, json, csv


```/invoice/download/csv```
   
eg.

   ```/invoice/download/csv```
   
<h3>How to run</h3>
run 

```docker-compose up```

and install packages with 

```composer install```

project is ready under

```http://localhost:8097```