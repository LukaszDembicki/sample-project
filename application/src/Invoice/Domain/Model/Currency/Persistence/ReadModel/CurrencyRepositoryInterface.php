<?php

declare(strict_types=1);

namespace App\Invoice\Domain\Model\Currency\Persistence\ReadModel;

use App\Invoice\Domain\Model\Currency\Currency;

interface CurrencyRepositoryInterface
{
    /**
     * @return Currency[]
     */
    public function getAll(): array;
}
