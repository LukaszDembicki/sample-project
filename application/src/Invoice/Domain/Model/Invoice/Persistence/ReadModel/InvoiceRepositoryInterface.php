<?php

declare(strict_types=1);

namespace App\Invoice\Domain\Model\Invoice\Persistence\ReadModel;

use App\Invoice\Domain\Model\Invoice\Invoice;

interface InvoiceRepositoryInterface
{
    /**
     * @param int $pageNumber
     * @param int|null $pageSize
     * @param string|null $orderBy
     * @return Invoice[]
     */
    public function getAll(int $pageNumber, ?int $pageSize, ?string $orderBy): array;
}