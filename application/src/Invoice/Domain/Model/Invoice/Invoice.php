<?php

namespace App\Invoice\Domain\Model\Invoice;

use App\Customer\Domain\Model\Customer\Customer;
use App\Invoice\Domain\Model\Currency\Currency;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table(name="invoice", indexes={@ORM\Index(name="FK_currency", columns={"currency"}), @ORM\Index(name="FK_recipient", columns={"recipient"}), @ORM\Index(name="FK_sender", columns={"sender"})})
 * @ORM\Entity
 */
class Invoice
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private DateTime $date;

    /**
     * @ORM\Column(name="value", type="decimal", precision=10, scale=0, nullable=false)
     */
    private float $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Invoice\Domain\Model\Currency\Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="currency", referencedColumnName="id")
     * })
     */
    private Currency $currency;

    /**
     * @ORM\ManyToOne(targetEntity="App\Customer\Domain\Model\Customer\Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recipient", referencedColumnName="id")
     * })
     */
    private Customer $recipient;

    /**
     * @ORM\ManyToOne(targetEntity="App\Customer\Domain\Model\Customer\Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sender", referencedColumnName="id")
     * })
     */
    private Customer $sender;

    private array $exchangeRateValues;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value): void
    {
        $this->value = $value;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency(Currency $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return Customer
     */
    public function getRecipient(): Customer
    {
        return $this->recipient;
    }

    /**
     * @param Customer $recipient
     */
    public function setRecipient(Customer $recipient): void
    {
        $this->recipient = $recipient;
    }

    /**
     * @return Customer
     */
    public function getSender(): Customer
    {
        return $this->sender;
    }

    /**
     * @param Customer $sender
     */
    public function setSender(Customer $sender): void
    {
        $this->sender = $sender;
    }

    public function getExchangeRateValues(): array
    {
        return $this->exchangeRateValues;
    }

    public function setExchangeRateValues(array $exchangeRateValues): void
    {
        $this->exchangeRateValues = $exchangeRateValues;
    }
}
