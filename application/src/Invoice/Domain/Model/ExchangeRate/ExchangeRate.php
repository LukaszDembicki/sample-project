<?php

declare(strict_types=1);

namespace App\Invoice\Domain\Model\ExchangeRate;

// todo move to yml
use Symfony\Component\Serializer\Annotation\Ignore;
use DateTime;

class ExchangeRate
{
    private string $currency;

    #[ignore]
    private DateTime $exchangeRateDate;

    private float $value;

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getExchangeRateDate(): DateTime
    {
        return $this->exchangeRateDate;
    }

    public function setExchangeRateDate(DateTime $exchangeRateDate): void
    {
        $this->exchangeRateDate = $exchangeRateDate;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue(float $value): void
    {
        $this->value = $value;
    }
}
