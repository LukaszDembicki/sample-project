<?php

declare(strict_types=1);

namespace App\Invoice\Domain\Model\ExchangeRate;

use App\Shared\Component\Collection\Collection;

class ExchangeRateCollection extends Collection
{
}