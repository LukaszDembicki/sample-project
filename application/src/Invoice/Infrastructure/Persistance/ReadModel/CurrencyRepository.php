<?php

declare(strict_types=1);

namespace App\Invoice\Infrastructure\Persistance\ReadModel;

use App\Invoice\Domain\Model\Currency\Currency;
use App\Invoice\Domain\Model\Currency\Persistence\ReadModel\CurrencyRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CurrencyRepository extends ServiceEntityRepository implements CurrencyRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Currency::class);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }
}
