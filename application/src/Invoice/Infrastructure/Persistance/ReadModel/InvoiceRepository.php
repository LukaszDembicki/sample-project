<?php

declare(strict_types=1);

namespace App\Invoice\Infrastructure\Persistance\ReadModel;

use App\Invoice\Domain\Model\Currency\Currency;
use App\Invoice\Domain\Model\Invoice\Invoice;
use App\Invoice\Domain\Model\Invoice\Persistence\ReadModel\InvoiceRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

class InvoiceRepository extends ServiceEntityRepository implements InvoiceRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Invoice::class);
    }

    public function getAll(int $pageNumber, ?int $pageSize, ?string $orderBy): array
    {
        // todo implement pagination, orderBy
        return $this->findAll();
    }
}