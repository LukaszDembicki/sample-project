<?php

declare(strict_types=1);

namespace App\Invoice\Application\Currency\Service;

use App\Invoice\Application\ExchangeRate\Converter\ExchangeRateConverter;
use App\Invoice\Application\ListInvoice\DTO\InvoiceDTOCollection;
use App\Invoice\Domain\Model\Currency\Currency;
use App\Invoice\Domain\Model\ExchangeRate\ExchangeRateCollection;
use App\Invoice\Domain\Model\Invoice\Invoice;
use App\Invoice\Infrastructure\Persistance\ReadModel\CurrencyRepository;

class AddExchangeRateValue
{
    private ExchangeRateConverter $exchangeRateConverter;

    private CurrencyRepository $currencyRepository;

    public function __construct(
        ExchangeRateConverter $exchangeRateConverter,
        CurrencyRepository $currencyRepository
    )
    {
        $this->exchangeRateConverter = $exchangeRateConverter;
        $this->currencyRepository = $currencyRepository;
    }

    public function addExchangeRateValueToInvoice(InvoiceDTOCollection $invoiceDTOs, array $exchangeRateList): void
    {
        $currencies = $this->currencyRepository->getAll();
        /** @var Invoice $invoice */
        foreach ($invoiceDTOs as $invoiceDTO) {
            /** @var ExchangeRateCollection $exchangeRates */
            $exchangeRates = $exchangeRateList[$invoiceDTO->getDate()->format('Y-m-d')] ?? null;
            if ($exchangeRates === null) {
                $invoiceDTO->setExchangeRateValues([]);
                continue;
            }

            $exchangeRateValues = [];
            /** @var Currency $currency */
            foreach ($currencies as $currency) {
                if (!$exchangeRates->containsKey($currency->getId())) {
                    $exchangeRateValues[$currency->getId()] = null;
                    continue;
                }

                $exchangeRateValues[$currency->getId()] = $this->exchangeRateConverter->convertFrom(
                    $invoiceDTO->getValue(),
                    $exchangeRates->get($currency->getId())->getValue()
                );
            }

            $invoiceDTO->setExchangeRateValues($exchangeRateValues);
        }
    }
}
