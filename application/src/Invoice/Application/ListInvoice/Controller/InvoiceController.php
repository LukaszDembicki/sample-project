<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\Controller;

use App\Invoice\Application\ListInvoice\Message\ListInvoiceMessage;
use App\Shared\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceController extends AbstractController
{
    private SerializerInterface $serializerService;

    public function __construct(SerializerInterface $serializerService)
    {
        $this->serializerService = $serializerService;
    }

    #[Route('/invoice/list', name: 'invoice_list')]
    public function list(Request $request, MessageBusInterface $bus): Response
    {
        $invoiceDTOCollection = $bus->dispatch(
            new ListInvoiceMessage(
                (int)$request->get('pageNumber') ?? 1,
                (int)$request->get('pageSize') ?? null,
                (string)$request->get('orderBy') ?? null
            )
        );

        $stamp = $invoiceDTOCollection->last(HandledStamp::class);

        return JsonResponse::fromJsonString($this->serializerService->serialize(
            $stamp->getResult(), 'json')
        );
    }
}
