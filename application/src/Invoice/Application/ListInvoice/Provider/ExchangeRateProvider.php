<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\Provider;

use App\Invoice\Application\ListInvoice\Transformer\ExchangeRateTransformer;
use App\Invoice\Domain\Model\Invoice\Invoice;
use App\Shared\Component\Client\ClientInterface;
use App\Shared\Component\Client\Exception\ClientException;
use DateTime;

class ExchangeRateProvider
{
    private const EXCHANGE_RATE_API = 'http://api.nbp.pl/api/exchangerates/tables/a/%s/%s/?format=json';

    private ClientInterface $client;

    private ExchangeRateTransformer $exchangeRateTransformer;

    public function __construct(
        ClientInterface $client,
        ExchangeRateTransformer $exchangeRateTransformer
    )
    {
        $this->client = $client;
        $this->exchangeRateTransformer = $exchangeRateTransformer;
    }

    /**
     * @param array $invoices
     * @return array
     * @throws ClientException
     */
    public function provideExchangeRates(array $invoices): array
    {
        // todo store exchange rates in nosql database + warmer

        $exchangeRate = [];
        $minDate = $this->getMinDate($invoices);
        $maxDate = $this->getMaxDate($invoices);
        while ($minDate <= $maxDate) {
            $requestFrom = $this->getRequestFrom($minDate);
            $requestTo = $this->getRequestTo($minDate);
            $exchangeRate[] = $this->client->get(sprintf(
                    self::EXCHANGE_RATE_API,
                    $requestFrom->format('Y-m-d'),
                    $requestTo->format('Y-m-d')
                )
            );

            $minDate->modify('+93 day');
        }

        return $this->exchangeRateTransformer->transformApiResponseToCollection(array_merge(...$exchangeRate));
    }

    private function getMinDate(array $invoices): DateTime
    {
        $minDate = new DateTime('now');
        /** @var Invoice $invoice */
        foreach ($invoices as $invoice) {
            if ($invoice->getDate() < $minDate) {
                $minDate = $invoice->getDate();
            }
        }

        return $minDate;
    }

    private function getMaxDate(array $invoices): ?DateTime
    {
        $maxDate = null;
        /** @var Invoice $invoice */
        foreach ($invoices as $invoice) {
            if ($invoice->getDate() > $maxDate) {
                $maxDate = $invoice->getDate();
            }
        }

        return $maxDate;
    }

    private function getRequestFrom(DateTime $minDate): DateTime
    {
        return clone $minDate;
    }

    private function getRequestTo(DateTime $minDate): DateTime
    {
        $requestTo = clone $minDate;
        $requestTo->modify('+93 day');

        $timeNow = new DateTime('now');
        if ($requestTo > $timeNow) {
            $requestTo = $timeNow;
        }

        return $requestTo;
    }
}
