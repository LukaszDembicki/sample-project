<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\Handler;

use App\Invoice\Application\ListInvoice\DTO\InvoiceDTOCollection;
use App\Invoice\Application\ListInvoice\Message\ListInvoiceMessage;
use App\Invoice\Application\ListInvoice\Service\GetInvoiceListService;
use App\Shared\Component\Client\Exception\ClientException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ListInvoiceHandler implements MessageHandlerInterface
{
    private GetInvoiceListService $getInvoiceListService;

    public function __construct(GetInvoiceListService $getInvoiceListService)
    {
        $this->getInvoiceListService = $getInvoiceListService;
    }

    /**
     * @param ListInvoiceMessage $listInvoiceMessage
     * @return InvoiceDTOCollection
     * @throws ClientException
     */
    public function __invoke(ListInvoiceMessage $listInvoiceMessage): InvoiceDTOCollection
    {
        return $this->getInvoiceListService->getInvoiceListWithExchangeRateValues(
            $listInvoiceMessage->getPageNumber(),
            $listInvoiceMessage->getPageSize(),
            $listInvoiceMessage->getOrderBy()
        );
    }
}