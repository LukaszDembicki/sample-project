<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\Message;

class ListInvoiceMessage
{
    private int $pageNumber = 1;
    private ?int $pageSize;
    private ?string $orderBy;

    public function __construct(int $pageNumber, ?int $pageSize, ?string $orderBy)
    {
        $this->pageNumber = $pageNumber;
        $this->pageSize = $pageSize;
        $this->orderBy = $orderBy;
    }

    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }

    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }
}
