<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\Service;

use App\Invoice\Application\Currency\Service\AddExchangeRateValue;
use App\Invoice\Application\ListInvoice\DTO\InvoiceDTOCollection;
use App\Invoice\Application\ListInvoice\Provider\ExchangeRateProvider;
use App\Invoice\Application\ListInvoice\Transformer\InvoiceListDTOTransformer;
use App\Invoice\Domain\Model\Invoice\Persistence\ReadModel\InvoiceRepositoryInterface;
use App\Shared\Component\Client\ClientInterface;
use App\Shared\Component\Client\Exception\ClientException;
use App\Shared\Component\Mapper\MapperInterface;

class GetInvoiceListService
{
    private ClientInterface $client;

    private InvoiceRepositoryInterface $invoiceRepository;

    private AddExchangeRateValue $addExchangeRateValueToInvoiceTransformer;

    private ExchangeRateProvider $exchangeRateProvider;

    private MapperInterface $mapper;

    private InvoiceListDTOTransformer $invoiceListDTOTransformer;

    public function __construct(
        ClientInterface $client,
        InvoiceRepositoryInterface $invoiceRepository,
        AddExchangeRateValue $addExchangeRateValueToInvoiceService,
        ExchangeRateProvider $exchangeRateProvider,
        MapperInterface $mapper,
        InvoiceListDTOTransformer $invoiceListDTOTransformer

    )
    {
        $this->client = $client;
        $this->invoiceRepository = $invoiceRepository;
        $this->addExchangeRateValueToInvoiceTransformer = $addExchangeRateValueToInvoiceService;
        $this->exchangeRateProvider = $exchangeRateProvider;
        $this->mapper = $mapper;
        $this->invoiceListDTOTransformer = $invoiceListDTOTransformer;
    }

    /**
     * @param int $pageNumber
     * @param int|null $pageSize
     * @param string|null $orderBy
     * @return InvoiceDTOCollection
     *
     * @throws ClientException
     */
    public function getInvoiceListWithExchangeRateValues(int $pageNumber, ?int $pageSize, ?string $orderBy): InvoiceDTOCollection
    {
        $invoices = $this->invoiceRepository->getAll(
            $pageNumber,
            $pageSize,
            $orderBy
        );

        $invoiceDTOCollection = $this->invoiceListDTOTransformer->transformArrayListToCollection($invoices);

        $this->addExchangeRateValueToInvoiceTransformer->addExchangeRateValueToInvoice(
            $invoiceDTOCollection,
            $this->exchangeRateProvider->provideExchangeRates($invoices)
        );

        return $invoiceDTOCollection;
    }
}
