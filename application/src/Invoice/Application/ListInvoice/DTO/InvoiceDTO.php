<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\DTO;

use App\Customer\Domain\Model\Customer\Customer;
use App\Invoice\Domain\Model\Currency\Currency;
use DateTime;

class InvoiceDTO
{
    private DateTime $date;

    private float $value;

    private Currency $currency;

    private Customer $recipient;

    private Customer $sender;

    private ?array $exchangeRateValues;

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue(float $value): void
    {
        $this->value = $value;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function setCurrency(Currency $currency): void
    {
        $this->currency = $currency;
    }

    public function getRecipient(): Customer
    {
        return $this->recipient;
    }

    public function setRecipient(Customer $recipient): void
    {
        $this->recipient = $recipient;
    }

    public function getSender(): Customer
    {
        return $this->sender;
    }

    public function setSender(Customer $sender): void
    {
        $this->sender = $sender;
    }

    public function getExchangeRateValues(): ?array
    {
        return $this->exchangeRateValues;
    }

    public function setExchangeRateValues(?array $exchangeRateValues): void
    {
        $this->exchangeRateValues = $exchangeRateValues;
    }
}
