<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\DTO;

use App\Shared\Component\Collection\Collection;

class InvoiceDTOCollection extends Collection
{
}
