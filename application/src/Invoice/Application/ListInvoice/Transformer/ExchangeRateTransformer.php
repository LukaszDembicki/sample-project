<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\Transformer;

use App\Invoice\Domain\Model\ExchangeRate\ExchangeRate;
use App\Invoice\Domain\Model\ExchangeRate\ExchangeRateCollection;
use DateTime;

class ExchangeRateTransformer
{
    /**
     * @param array $exchangeRateTableList
     * @return ExchangeRateCollection[]
     */
    public function transformApiResponseToCollection(array $exchangeRateTableList): array
    {
        $exchangeRateCollectionPerDateList = [];
        foreach ($exchangeRateTableList as $exchangeRateTable) {
            $exchangeRatesCollection = new ExchangeRateCollection();
            foreach ($exchangeRateTable['rates'] as $exchangeRateKey => $exchangeRate) {
                $exchangeRateModel = new ExchangeRate();
                $exchangeRateModel->setCurrency($exchangeRate['code']);
                $exchangeRateModel->setValue($exchangeRate['mid']);
                $exchangeRateModel->setExchangeRateDate(DateTime::createFromFormat('Y-m-d', $exchangeRateTable['effectiveDate']));

                $exchangeRatesCollection->set($exchangeRate['code'], $exchangeRateModel);
            }

            $exchangeRateCollectionPerDateList[$exchangeRateTable['effectiveDate']] = $exchangeRatesCollection;
        }

        return $exchangeRateCollectionPerDateList;
    }
}
