<?php

declare(strict_types=1);

namespace App\Invoice\Application\ListInvoice\Transformer;

use App\Invoice\Application\ListInvoice\DTO\InvoiceDTO;
use App\Invoice\Application\ListInvoice\DTO\InvoiceDTOCollection;
use App\Invoice\Domain\Model\Invoice\Invoice;
use App\Shared\Component\Mapper\MapperInterface;

class InvoiceListDTOTransformer
{
    private MapperInterface $mapper;

    public function __construct(MapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    public function transformArrayListToCollection(array $invoiceList): InvoiceDTOCollection
    {
        $invoiceDTOCollection = new InvoiceDTOCollection();
        /** @var Invoice $invoice */
        foreach ($invoiceList as $invoice) {
            $invoiceDTO = new InvoiceDTO();
            $this->mapper->mapFromObject($invoice, $invoiceDTO);
            $invoiceDTOCollection->add($invoiceDTO);
        }

        return $invoiceDTOCollection;
    }
}
