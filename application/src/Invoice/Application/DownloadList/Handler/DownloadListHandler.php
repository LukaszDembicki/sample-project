<?php

declare(strict_types=1);

namespace App\Invoice\Application\DownloadList\Handler;

use App\Invoice\Application\DownloadList\FileFormatConverter\FileFormatFactory;
use App\Invoice\Application\DownloadList\Message\DownloadListMessage;
use App\Invoice\Application\ListInvoice\Message\ListInvoiceMessage;
use App\Invoice\Application\ListInvoice\Service\GetInvoiceListService;
use App\Shared\Component\Client\Exception\ClientException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class DownloadListHandler implements MessageHandlerInterface
{
    private MessageBusInterface $messageBus;

    private FileFormatFactory $fileFormatFactory;

    private GetInvoiceListService $getInvoiceListService;

    public function __construct(
        MessageBusInterface $messageBus,
        FileFormatFactory $fileFormatFactory,
        GetInvoiceListService $getInvoiceListService
    )
    {
        $this->messageBus = $messageBus;
        $this->fileFormatFactory = $fileFormatFactory;
        $this->getInvoiceListService = $getInvoiceListService;
    }

    /**
     * @param DownloadListMessage $downloadListMessage
     * @return string
     * @throws ClientException
     */
    public function __invoke(DownloadListMessage $downloadListMessage): string
    {
        $invoiceDTOCollection = $this->messageBus->dispatch(
            new ListInvoiceMessage(
                $downloadListMessage->getPageNumber(),
                $downloadListMessage->getPageSize(),
                $downloadListMessage->getOrderBy()
            )
        );

        $fileFormatConverter = $this->fileFormatFactory->create($downloadListMessage->getFileFormat());
        return $fileFormatConverter->convertFromCollection($invoiceDTOCollection->last(HandledStamp::class)->getResult());
    }
}