<?php

declare(strict_types=1);

namespace App\Invoice\Application\DownloadList\Controller;

use App\Invoice\Application\DownloadList\Message\DownloadListMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

class DownloadListController extends AbstractController
{
    #[Route('/invoice/download/{type}', name: 'invoice_list_download')]
    public function download(Request $request, MessageBusInterface $bus): Response
    {
        $invoiceList = $bus->dispatch(
            new DownloadListMessage(
                (int)$request->get('pageNumber') ?? 1,
                (int)$request->get('pageSize') ?? null,
                (string)$request->get('orderBy') ?? null,
                $request->get('type')
            )
        );

        $stamp = $invoiceList->last(HandledStamp::class);
        $fileToDownload = $stamp->getResult();
        $response = new Response($fileToDownload);

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            sprintf('invoiceList.%s', $request->get('type'))
        );

        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Connection', 'Keep-Alive');

        return $response;
    }
}
