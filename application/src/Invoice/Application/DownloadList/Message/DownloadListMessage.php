<?php

declare(strict_types=1);

namespace App\Invoice\Application\DownloadList\Message;

class DownloadListMessage
{
    private int $pageNumber = 1;
    private ?int $pageSize;
    private ?string $orderBy;
    private string $fileFormat;

    public function __construct(int $pageNumber, ?int $pageSize, ?string $orderBy, string $fileFormat)
    {
        $this->pageNumber = $pageNumber;
        $this->pageSize = $pageSize;
        $this->orderBy = $orderBy;
        $this->fileFormat = $fileFormat;
    }

    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }

    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    public function getFileFormat(): string
    {
        return $this->fileFormat;
    }
}