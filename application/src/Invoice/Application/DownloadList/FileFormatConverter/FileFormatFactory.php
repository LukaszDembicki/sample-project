<?php

declare(strict_types=1);

namespace App\Invoice\Application\DownloadList\FileFormatConverter;

use App\Shared\Component\Serializer\SerializerInterface;

class FileFormatFactory
{
    protected SerializerInterface $serializerService;

    public function __construct(SerializerInterface $serializerService)
    {
        $this->serializerService = $serializerService;
    }

    public function create(string $type = 'json'): AbstractFileFormatConverter
    {
        return match ($type) {
            'csv' => new CsvFileFormatConverter($this->serializerService),
            default => new JsonFileFormatConverter($this->serializerService),
        };
    }
}
