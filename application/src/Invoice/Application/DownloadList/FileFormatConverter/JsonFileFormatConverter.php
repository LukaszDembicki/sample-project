<?php

declare(strict_types=1);

namespace App\Invoice\Application\DownloadList\FileFormatConverter;

use App\Invoice\Application\ListInvoice\DTO\InvoiceDTOCollection;

class JsonFileFormatConverter extends AbstractFileFormatConverter
{
    public function convertFromCollection(InvoiceDTOCollection $invoiceDTOCollection): string
    {
        return $this->serializerService->serialize(
            $invoiceDTOCollection,
            'json',
            ['json_encode_options' => \JSON_PRESERVE_ZERO_FRACTION]
        );
    }
}
