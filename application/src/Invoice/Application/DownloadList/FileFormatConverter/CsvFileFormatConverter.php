<?php

declare(strict_types=1);

namespace App\Invoice\Application\DownloadList\FileFormatConverter;

use App\Invoice\Application\ListInvoice\DTO\InvoiceDTOCollection;

class CsvFileFormatConverter extends AbstractFileFormatConverter
{
    public function convertFromCollection(InvoiceDTOCollection $invoiceDTOCollection): string
    {
        return $this->serializerService->serialize(
            $invoiceDTOCollection,
            'csv'
        );
    }
}
