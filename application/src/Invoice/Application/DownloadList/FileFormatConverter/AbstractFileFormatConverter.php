<?php

declare(strict_types=1);

namespace App\Invoice\Application\DownloadList\FileFormatConverter;

use App\Invoice\Application\ListInvoice\DTO\InvoiceDTOCollection;
use App\Shared\Component\Serializer\SerializerInterface;

abstract class AbstractFileFormatConverter
{
    protected SerializerInterface $serializerService;

    public function __construct(SerializerInterface $serializerService)
    {
        $this->serializerService = $serializerService;
    }

    public abstract function convertFromCollection(InvoiceDTOCollection $invoiceDTOCollection): string;
}
