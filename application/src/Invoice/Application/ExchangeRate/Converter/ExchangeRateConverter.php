<?php

declare(strict_types=1);

namespace App\Invoice\Application\ExchangeRate\Converter;

class ExchangeRateConverter
{
    public function convertFrom(float $value, float $exchangeRate): float
    {
        return $value * $exchangeRate;
    }
}
