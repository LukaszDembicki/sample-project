<?php

declare(strict_types=1);

namespace App\Shared\Component\Collection;

use Doctrine\Common\Collections\ArrayCollection;

class Collection extends ArrayCollection implements CollectionInterface
{
    public function set($key, $value)
    {
        parent::set($key, $value);
    }

    public function add($element)
    {
        parent::add($element);
    }

    public function remove($key)
    {
        return parent::remove($key);
    }

    public function containsKey($key)
    {
        return parent::containsKey($key);
    }
}
