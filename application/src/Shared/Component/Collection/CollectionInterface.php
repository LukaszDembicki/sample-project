<?php

declare(strict_types=1);

namespace App\Shared\Component\Collection;

interface CollectionInterface
{
    public function set($key, $value);

    public function add($element);

    public function remove($key);

    public function containsKey($key);
}