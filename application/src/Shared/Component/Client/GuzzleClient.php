<?php

declare(strict_types=1);

namespace App\Shared\Component\Client;

use App\Shared\Component\Client\Exception\ClientException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GuzzleClient implements ClientInterface
{
    public function get(string $url): array
    {
        $client = new Client();

        try {
            $response = $client->request('GET', $url);
        } catch (GuzzleException $exception) {
            throw new ClientException($exception->getMessage());
        }

        return json_decode($response->getBody()->getContents(), true) ?? [];
    }
}
