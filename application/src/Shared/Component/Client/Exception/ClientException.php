<?php

declare(strict_types=1);

namespace App\Shared\Component\Client\Exception;

use Exception;

class ClientException extends Exception
{
}
