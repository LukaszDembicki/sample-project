<?php

declare(strict_types=1);

namespace App\Shared\Component\Client;

use App\Shared\Component\Client\Exception\ClientException;

interface ClientInterface
{
    /**
     * @throws ClientException
     *
     * @param string $url
     * @return array
     */
    public function get(string $url): array;
}