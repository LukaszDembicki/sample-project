<?php

declare(strict_types=1);

namespace App\Shared\Component\Mapper;

use App\Shared\Component\Collection\CollectionInterface;
use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;

class Mapper implements MapperInterface
{
    private AutoMapperConfig $autoMapperConfig;

    private AutoMapper $autoMapper;

    public function __construct()
    {
        $this->autoMapperConfig = new AutoMapperConfig();
        $this->autoMapper = new AutoMapper($this->autoMapperConfig);
    }

    public function map($sourceObject, string $className, array $ignored = [], array $included = [])
    {
        $object = new $className();
        return $this->mapFromObject(
            $sourceObject,
            $object,
            $ignored,
            $included
        );
    }

    public function mapFromObject($sourceObject, $destinationObject, array $ignored = [], array $included = [])
    {
        $this->autoMapperConfig->registerMapping(get_class($sourceObject), get_class($destinationObject));

        return $this->autoMapper->mapToObject($sourceObject, $destinationObject);
    }

    public function mapArrayToCollection(array $arrayList, CollectionInterface $collection): CollectionInterface
    {
        foreach ($arrayList as $array) {
            $collection->add($array);
        }

        return $collection;
    }
}
