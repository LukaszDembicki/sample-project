<?php

declare(strict_types=1);

namespace App\Shared\Component\Mapper;

interface MapperInterface
{
    public function map($sourceObject, string $className, array $ignored = [], array $included = []);

    public function mapFromObject($sourceObject, $destinationObject, array $ignored = [], array $included = []);
}