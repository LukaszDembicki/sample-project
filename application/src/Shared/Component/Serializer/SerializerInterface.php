<?php

declare(strict_types=1);

namespace App\Shared\Component\Serializer;

interface SerializerInterface
{
    public function serialize($dataToSerialize, string $format, ?array $context = []);
}
