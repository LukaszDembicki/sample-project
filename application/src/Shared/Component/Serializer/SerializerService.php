<?php

declare(strict_types=1);

namespace App\Shared\Component\Serializer;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializerService implements SerializerInterface
{
    private const IGNORED_ATTRIBUTES = ['__initializer__', '__cloner__', '__isInitialized__', 'keys', 'iterator'];

    public function serialize($dataToSerialize, string $format, ?array $context = [])
    {
        $context = $this->addIgnoredAttributesToContext($context);
        $objectNormalizer = $this->getObjectNormalizer($context);

        $serializer = new Serializer([$objectNormalizer], [$this->getEncoderByType($format)]);

        return $serializer->serialize($dataToSerialize, $format, [
            $context
        ]);
    }

    /**
     * @param array $data
     * @param string|null $object $object
     * @param string|null $format
     * @param array|null $context
     * @return object
     * @throws ExceptionInterface
     */
    public function denormalize(array $data, ?string $object, ?string $format = null, ?array $context = []): object
    {
        $normalizer = new ObjectNormalizer();
        $serializer = new Serializer([$normalizer]);

        $normalizer->setSerializer($serializer);

        return $normalizer->denormalize($data, $object, $format, $context);
    }

    /**
     * @param object $object
     * @param string|null $format
     * @param array|null $context
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize($object, ?string $format = 'array', ?array $context = []): array
    {
        $context = $this->addIgnoredAttributesToContext($context);
        $normalizer = $this->getObjectNormalizer($context);

        $serializer = new Serializer([$normalizer]);
        $normalizer->setSerializer($serializer);

        return $normalizer->normalize($object, $format, $context);
    }

    private function addIgnoredAttributesToContext(?array $context): ?array
    {
        if (!isset($context[AbstractNormalizer::IGNORED_ATTRIBUTES])) {
            $context[AbstractNormalizer::IGNORED_ATTRIBUTES] = self::IGNORED_ATTRIBUTES;
        } else {
            foreach (self::IGNORED_ATTRIBUTES as $ignoredAttribute) {
                $context[AbstractNormalizer::IGNORED_ATTRIBUTES][] = $ignoredAttribute;
            }
        }
        return $context;
    }

    private function getObjectNormalizer(array $context): ObjectNormalizer
    {
        $dateCallback = function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
            return $innerObject instanceof \DateTime ? $innerObject->format(\DateTime::ISO8601) : '';
        };

        $defaultContext = [
            AbstractNormalizer::CALLBACKS => [
                'date' => $dateCallback,
            ],
        ];

        $context = array_merge($defaultContext, $context);

        $classMetadataFactory = new ClassMetadataFactory((new AnnotationLoader(new AnnotationReader())));
        return new ObjectNormalizer(classMetadataFactory: $classMetadataFactory, defaultContext: $context);
    }

    private function getEncoderByType(string $type): EncoderInterface
    {
        return match ($type) {
            'csv' => new CsvEncoder(),
            default => new JsonEncoder(),
        };
    }
}
