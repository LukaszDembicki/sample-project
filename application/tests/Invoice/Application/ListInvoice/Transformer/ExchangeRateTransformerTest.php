<?php

declare(strict_types=1);

namespace App\Tests\Invoice\Application\ListInvoice\Transformer;

use App\Invoice\Application\ListInvoice\Transformer\ExchangeRateTransformer;
use App\Invoice\Domain\Model\ExchangeRate\ExchangeRate;
use App\Invoice\Domain\Model\ExchangeRate\ExchangeRateCollection;
use PHPUnit\Framework\TestCase;

class ExchangeRateTransformerTest extends TestCase
{
    private ExchangeRateTransformer $exchangeRateTransformer;

    public function setUp(): void
    {
        $this->exchangeRateTransformer = new ExchangeRateTransformer();
    }

    public function testWillTransformApiResponseToCollection(): void
    {
        $this->assertEquals(
            $this->expectedOutput(),
            $this->exchangeRateTransformer->transformApiResponseToCollection($this->inputData())
        );
    }

    private function inputData(): array
    {
        return [
            [
                'table' => 'A',
                'no' => '069/A/NBP/2021',
                'effectiveDate' => '2021-04-12',
                'rates' => [
                    [
                        'currency' => 'bat (Tajlandia)',
                        'code' => 'THB',
                        'mid' => 0.1212
                    ]
                ]
            ]
        ];
    }

    private function expectedOutput(): array
    {
        $exchangeRate = new ExchangeRate();
        $exchangeRate->setValue(0.1212);
        $exchangeRate->setCurrency('THB');
        $exchangeRate->setExchangeRateDate(\DateTime::createFromFormat('Y-m-d', '2021-04-12'));

        $exchangeRateCollection = new ExchangeRateCollection();
        $exchangeRateCollection->set('THB', $exchangeRate);

        return [
            '2021-04-12' => $exchangeRateCollection
        ];
    }
}
