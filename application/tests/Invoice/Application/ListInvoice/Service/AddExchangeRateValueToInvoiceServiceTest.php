<?php

declare(strict_types=1);

namespace App\Tests\Invoice\Application\ListInvoice\Service;

use App\Invoice\Application\ExchangeRate\Converter\ExchangeRateConverter;
use App\Invoice\Application\Currency\Service\AddExchangeRateValue;
use App\Invoice\Domain\Model\Currency\Currency;
use App\Invoice\Domain\Model\ExchangeRate\ExchangeRate;
use App\Invoice\Domain\Model\ExchangeRate\ExchangeRateCollection;
use App\Invoice\Application\ListInvoice\DTO\InvoiceDTOCollection;
use App\Invoice\Domain\Model\Invoice\Invoice;
use App\Invoice\Infrastructure\Persistance\ReadModel\CurrencyRepository;
use PHPUnit\Framework\TestCase;

class AddExchangeRateValueToInvoiceServiceTest extends TestCase
{
    private AddExchangeRateValue $addExchangeRateValueToInvoiceService;

    private ExchangeRateConverter $exchangeRateConverter;

    private CurrencyRepository $currencyRepository;

    public function setUp(): void
    {
        $this->exchangeRateConverter = $this->createMock(ExchangeRateConverter::class);
        $this->currencyRepository = $this->createMock(CurrencyRepository::class);

        $this->addExchangeRateValueToInvoiceService = new AddExchangeRateValue(
            $this->exchangeRateConverter,
            $this->currencyRepository
        );
    }

    public function testWillFetchAllCurrencyList(): void
    {
        $this->currencyRepository->expects($this->once())->method('getAll');

        $this->addExchangeRateValueToInvoiceService->addExchangeRateValueToInvoice(
            $this->mockInvoiceCollectionData(),
            $this->mockExchangeRateListData()
        );
    }

    public function testWillSetEmptyExchangeRateValuesIfNoExchangeRateFound(): void
    {
        // MOCK
        $invoiceListDTO = new InvoiceDTOCollection();
        $invoice = $this->createMock(Invoice::class);
        $invoiceListDTO->add($invoice);

        // ASSERT
        $invoice->expects($this->once())->method('setExchangeRateValues');

        // ACT
        $this->addExchangeRateValueToInvoiceService->addExchangeRateValueToInvoice(
            $invoiceListDTO,
            $this->mockExchangeRateListData()
        );
    }

    public function testWillConvertInvoiceByGivenExchangeRate(): void
    {
        // MOCK
        $invoiceListDTO = $this->mockInvoiceCollectionData();
        $currency = new Currency();
        $currency->setId('THB');
        $this->currencyRepository->method('getAll')->willReturn([$currency]);

        // ASSERT
        $this->exchangeRateConverter->expects($this->once())->method('convertFrom');

        // ACT
        $this->addExchangeRateValueToInvoiceService->addExchangeRateValueToInvoice(
            $invoiceListDTO,
            $this->mockExchangeRateListData()
        );
    }

    private function mockInvoiceCollectionData(): InvoiceDTOCollection
    {
        $invoiceListDTO = new InvoiceDTOCollection();

        $invoice = new Invoice();
        $invoice->setId(1);
        $invoice->setValue(123.3);
        $invoice->setDate(\DateTime::createFromFormat('Y-m-d', '2021-04-12'));
        $invoiceListDTO->add($invoice);

        return $invoiceListDTO;
    }

    private function mockExchangeRateListData(): array
    {
        $exchangeRate = new ExchangeRate();
        $exchangeRate->setValue(0.1212);
        $exchangeRate->setCurrency('THB');
        $exchangeRate->setExchangeRateDate(\DateTime::createFromFormat('Y-m-d', '2021-04-12'));

        $exchangeRateCollection = new ExchangeRateCollection();
        $exchangeRateCollection->set('THB', $exchangeRate);

        return [
            '2021-04-12' => $exchangeRateCollection
        ];
    }

}