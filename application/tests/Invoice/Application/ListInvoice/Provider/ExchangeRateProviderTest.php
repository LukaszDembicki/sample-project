<?php

declare(strict_types=1);

namespace App\Tests\Invoice\Application\ListInvoice\Provider;

use App\Invoice\Application\ListInvoice\Provider\ExchangeRateProvider;
use App\Invoice\Application\ListInvoice\Transformer\ExchangeRateTransformer;
use App\Invoice\Domain\Model\ExchangeRate\ExchangeRate;
use App\Invoice\Domain\Model\Invoice\Invoice;
use App\Shared\Component\Client\ClientInterface;
use DateTime;
use PHPUnit\Framework\TestCase;

class ExchangeRateProviderTest extends TestCase
{
    private ExchangeRateProvider $exchangeRateProvider;

    private ExchangeRateTransformer $exchangeRateTransformer;

    private ClientInterface $client;

    public function setUp(): void
    {
        $this->client = $this->createMock(ClientInterface::class);
        $this->exchangeRateTransformer = new ExchangeRateTransformer();

        $this->exchangeRateProvider = new ExchangeRateProvider($this->client, $this->exchangeRateTransformer);
    }

    public function testWillReturnEmptyArrayWhenNoResultsGiven(): void
    {
        $this->client->method('get')->willReturn(
            $this->mockInputData()
        );

        $this->assertEquals([], $this->exchangeRateProvider->provideExchangeRates([]));
    }

    public function testProvideExchangeRatesWillReturnValidFormat(): void
    {
        // MOCK
        $this->client->method('get')->willReturn(
            $this->mockInputData()
        );

        // ASSERT
        $this->client->expects($this->atLeastOnce())->method('get');

        // ACT
        $output = $this->exchangeRateProvider->provideExchangeRates($this->getInputDataSet());

        // todo assert output
//        $this->assertEquals(
//            $this->exchangeRateProvider->provideRangeDates($this->getInputDataSet()),
//            $this->expectedOutput()
//        );
    }

    private function getInputDataSet(): array
    {
        $inputDates = [
            new DateTime('now'),
            (new DateTime('now'))->modify('-1 month'),
            (new DateTime('now'))->modify('-2 month'),
        ];

        $invoices = [];
        foreach ($inputDates as $date) {
            $invoice = new Invoice();
            $invoice->setId(rand(1, 1000));
            $invoice->setDate($date);

            $invoices[] = $invoice;
        }

        return $invoices;
    }

    private function mockInputData(): array
    {
        return [
            [
                'table' => 'A',
                'no' => '069/A/NBP/2021',
                'effectiveDate' => (new DateTime('now'))->modify('-2 month')->format('Y-m-d'),
                'rates' => [
                    [
                        'currency' => 'bat (Tajlandia)',
                        'code' => 'THB',
                        'mid' => 0.1212
                    ],
                    [
                        'currency' => 'dolar amerykański',
                        'code' => 'USD',
                        'mid' => 3.8144
                    ],
                    [
                        'currency' => 'dolar austrialijski',
                        'code' => 'AUD',
                        'mid' => 2.9053
                    ]
                ]
            ],
            [
                'table' => 'A',
                'no' => '069/A/NBP/2021',
                'effectiveDate' => (new DateTime('now'))->modify('-1 month')->format('Y-m-d'),
                'rates' => [
                    [
                        'currency' => 'bat (Tajlandia)',
                        'code' => 'THB',
                        'mid' => 0.1214
                    ],
                    [
                        'currency' => 'dolar amerykański',
                        'code' => 'USD',
                        'mid' => 3.8342
                    ],
                    [
                        'currency' => 'dolar austrialijski',
                        'code' => 'AUD',
                        'mid' => 2.9202
                    ]
                ]
            ],
            [
                'table' => 'A',
                'no' => '069/A/NBP/2021',
                'effectiveDate' => (new DateTime('now'))->format('Y-m-d'),
                'rates' => [
                    [
                        'currency' => 'bat (Tajlandia)',
                        'code' => 'THB',
                        'mid' => 0.1216
                    ],
                    [
                        'currency' => 'dolar amerykański',
                        'code' => 'USD',
                        'mid' => 3.8301
                    ],
                    [
                        'currency' => 'dolar austrialijski',
                        'code' => 'AUD',
                        'mid' => 2.9232
                    ]
                ]
            ]
        ];
    }

    private function mockExchangeRate(string $currency, float $value, DateTime $exchangeRateDate): ExchangeRate
    {
        $exchangeRate = new ExchangeRate();
        $exchangeRate->setCurrency($currency);
        $exchangeRate->setValue($value);
        $exchangeRate->setExchangeRateDate($exchangeRateDate);

        return $exchangeRate;
    }
}
